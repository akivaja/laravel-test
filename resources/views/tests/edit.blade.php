<h1>Edit user</h1>
<form method = 'post' action="{{action('TestController@update', $test->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">User to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$test->title}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="save changes">
</div>

</form>
<form method = 'post' action="{{action('TestController@destroy', $test->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete user">
</div>

</form>
