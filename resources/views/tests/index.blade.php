@extends('layouts.app')

@section('content')

<h1>This is your User list</h1>
<ul>
    @foreach($tests as $test)
    <li>
        <!-- > id: {{$test->id}} title:{{$test->title}} <!-->
       @if ($test->status)
           <input type = 'checkbox' id ="{{$test->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$test->id}}">
       @endif
       <a href= "{{route('tests.edit', $test->id )}}"> {{$test->title}} </a>
    </li>
   
    @endforeach
</ul>

<a href="{{route('tests.create')}}">Create New User </a>
@endsection
