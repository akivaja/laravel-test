<?php

use Illuminate\Database\Seeder;

class TestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tests')->insert(
[
	[
            'title' => 'a@a.com',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
	],
	[
            'title' => 'b@b.com',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
	],
	[
            'title' => 'c@c.com',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
	],

        ]);
    }

}
