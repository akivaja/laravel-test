<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'jack',
                        'email' => 'a@a.com',
                        'password' => '12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'john',
                        'email' => 'b@b.com',
                        'password' => '12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'ron',
                        'email' => 'c@c.com',
                        'password' => '12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
            
                    ]);
    }

    }

